#include <iostream>
#include <vector>
#include <algorithm>
#include <memory>
#include <random>

class Animal
{
public:
	virtual ~Animal() = 0 {};
	virtual void Voice() const = 0;
};

class Dog : public Animal
{
public:
	virtual ~Dog() {}
	virtual void Voice() const override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	virtual ~Cat() {}
	virtual void Voice() const override
	{
		std::cout << "Mew!" << std::endl;
	}
};

class Fish : public Animal
{
public:
	virtual ~Fish() {}
	virtual void Voice() const override
	{
		std::cout << "... ..." << std::endl;
	}
};

class Mystery : public Animal
{
public:
	virtual ~Mystery() {}
	virtual void Voice() const override
	{
		std::cout << "The quick brown fox jumps over the lazy dog" << std::endl;
	}
};

enum AnimalTypeEnum
{
	atDog = 0,
	atCat,
	atFish
};

int main()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<int> dist(AnimalTypeEnum::atDog, AnimalTypeEnum::atFish);

	const auto animalsCount = 20;

	// Массив (вектор) для животных, хранит константные указатели на базовый класс
	std::vector<std::unique_ptr<const Animal>> animals;
	animals.reserve(animalsCount);	// Для предотвращения перевыделения памяти

	// Заполняем случайными потомками, количеством animalsCount
	std::generate_n(std::back_inserter(animals), animalsCount, [&dist, &mt]()
		{
			auto randomAnimalType = static_cast<AnimalTypeEnum>(dist(mt));

			switch (randomAnimalType)
			{
			case AnimalTypeEnum::atDog:
				return std::unique_ptr<const Animal>(new Dog);
			case AnimalTypeEnum::atCat:
				return std::unique_ptr<const Animal>(new Cat);
			case AnimalTypeEnum::atFish:
				return std::unique_ptr<const Animal>(new Fish);

			default:
				return std::unique_ptr<const Animal>(new Mystery);	// Сюда никогда не должны попасть
			}
		});

	// Вызываем Voice для всех животных
	for (const auto& a : animals)
		a->Voice();
}